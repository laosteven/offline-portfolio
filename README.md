# Offline Portfolio #
-------

Create an offline portfolio using **HTML/CSS** which contains a minimum of 4 pages:

* Introduction
* Portfolio
* Curriculum Vitae
* About me

Experiment and test color harmony in order to make a readable and easy-to-navigate website for the audience.

# Preview #
-------

![0532.png](https://bitbucket.org/repo/8k6L7M/images/857063915-0532.png)

# For more information #
-------
Visit the following website: [**Non-transactional websites conception** (TCH053)](https://cours.etsmtl.ca/tch053/) [*fr*]